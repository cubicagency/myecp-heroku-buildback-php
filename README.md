# Heroku buildpack: PHP with HTTP/2 support for cURL

## Description

This adds some modifications to the [official Heroku buildpack for PHP](https://github.com/heroku/heroku-buildpack-php).

PHP is compiled with a custom cURL which is compiled with HTTP/2 support using [nghttp2](https://nghttp2.org/).

The openssl-1.0.2 was added to support the ALPN protocol.

## Requirements

- [Docker](https://www.docker.com/): to create a *Heroku Cedar 14* PHP build container
- Amazon S3: to store the built packages

## Instructions

### 1. Get updates from the official buildpack

Add the upstream remote and fetch latest version:

    $ git remote add upstream https://github.com/heroku/heroku-buildpack-php.git
    $ git fetch upstream

Find the most recent git tag from the upstream git repo and merge it into master:

    $ git merge --no-ff vXXX

### 2. Build PHP

Build the Docker container:

    $ docker build --tag heroku-php-build-cedar-14 --file $(pwd)/support/build/_docker/cedar-14.Dockerfile .

Notes:

- see [README in `support/build/_docker/`](support/build/_docker/README.md) for more details.

Run bash inside it:

    $ docker run -it --env-file support/build/_docker/env.default \
        -e AWS_ACCESS_KEY_ID=<aws key id> \
        -e AWS_SECRET_ACCESS_KEY=<aws secret access key> \
        -e S3_BUCKET=<s3 bucket> \
        -e S3_PREFIX=dist-cedar-14-develop/ \
        heroku-php-build-cedar-14 \
        /bin/bash

Notes:

- see [README in `support/build/`](support/build/README.md) for details about `S3_PREFIX`.
- hint: the [Bucketeer](https://elements.heroku.com/addons/bucketeer) Heroku addon can be used to set up an Amazon S3 bucket.

Build PHP inside the container an deploy it to the bucket:

    $ support/build/_util/deploy.sh --publish php-5.6.22 --overwrite

Notes:

- see [README in `support/build/`](support/build/README.md) for details about [Bob](http://github.com/kennethreitz/bob-builder), builds and manifests

### 3. Usage in Applications

>   Please refer to [the instructions in the main README](#custom-platform-repositories) for details on how to use a custom repository during application builds.

Basically you need to set the `HEROKU_PHP_PLATFORM_REPOSITORIES` config var to the URL you got from the output of the `build.sh` command.

===

Content of the original README.md follows:

# Heroku buildpack: PHP

![php](https://cloud.githubusercontent.com/assets/51578/8882982/73ea501a-3219-11e5-8f87-311e6b8a86fc.jpg)


This is the official [Heroku buildpack](http://devcenter.heroku.com/articles/buildpacks) for PHP applications.

It uses Composer for dependency management, supports PHP or HHVM (experimental) as runtimes, and offers a choice of Apache2 or Nginx web servers.

## Usage

You'll need to use at least an empty `composer.json` in your application.

    $ echo '{}' > composer.json
    $ git add composer.json
    $ git commit -m "add composer.json for PHP app detection"

If you also have files from other frameworks or languages that could trigger another buildpack to detect your application as one of its own, e.g. a `package.json` which might cause your code to be detected as a Node.js application even if it is a PHP application, then you need to manually set your application to use this buildpack:

    $ heroku buildpacks:set heroku/php

This will use the officially published version. To use the `master` branch from GitHub instead:

    $ heroku buildpacks:set https://github.com/heroku/heroku-buildpack-php

Please refer to [Dev Center](https://devcenter.heroku.com/categories/php) for further usage instructions.

## Custom Platform Repositories

The buildpack uses Composer repositories to resolve platform (`php`, `hhvm`, `ext-something`, ...) dependencies.

To use a custom Composer repository with additional or different platform packages, add the URL to its `packages.json` to the `HEROKU_PHP_PLATFORM_REPOSITORIES` config var:

    $ heroku config:set HEROKU_PHP_PLATFORM_REPOSITORIES="https://mybucket.s3.amazonaws.com/cedar-14/packages.json"

To allow the use of multiple custom repositories, the config var may hold a list of multiple repository URLs, separated by a space character, in ascending order of precedence.

If the first entry in the list is "`-`" instead of a URL, the default platform repository is disabled entirely. This can be useful when testing development repositories, or to forcefully prevent the use of unwanted packages from the default platform repository.

For instructions on how to build custom platform packages (and a repository to hold them), please refer to the instructions [further below](#custom-platform-packages-and-repositories).

**Please note that Heroku cannot provide support for issues related to custom platform repositories and packages.**

## Development

The following information only applies if you're forking and hacking on this buildpack for your own purposes.

### Pull Requests

Please submit all pull requests against `develop` as the base branch.

### Custom Platform Packages and Repositories

Please refer to the [README in `support/build/`](support/build/README.md) for instructions.

